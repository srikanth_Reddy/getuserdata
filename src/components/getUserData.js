import React, { Component } from 'react';
import GetPost from './getPost';
import GetPhoto from './getPhoto';
import Div from './div'
import Address from './address';
import './getUserData.css'

class Display extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else
      return <>{
        items.map((x) => {
          return (
            <div className='header'>
              <div className='userInfo1'>
                <h2> <Div name="Name" value={x.name} /></h2>
                <Div name="email" value={x.email} />
                <Div name="phone" value={x.phone} />
                <Address name="address" value={x.address} />
                <hr />
                <br />
              </div>
              <div className='main'>
                <div className='leftBlock'>
                  <div className='userInfo'>
                    <h2> <Div name="Name" value={x.name} /></h2>
                    <Div name="email" value={x.email} />
                    <Div name="phone" value={x.phone} />
                    <Address name="address" value={x.address} />
                  </div>
                  <GetPhoto id={x.id} name="thumbnailUrl" />
                  <GetPhoto id={x.id} name="url" />
                </div>

                <GetPost id={x.id} />

              </div>
            </div>
          )

        }
        )
      }</>


  }
}


export default Display; 