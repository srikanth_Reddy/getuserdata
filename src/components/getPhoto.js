import React, { Component } from 'react';
import Div from './div'
import './getPhoto.css'

export default class getPhoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  componentDidMount() {
    let a = this.props.id
    fetch(` https://jsonplaceholder.typicode.com/photos?albumId=${a}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.slice(0, 5)
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      if (this.props.name === "thumbnailUrl") {
        return (
          <>
            <h1>Thumbnails</h1>
            <div className='thumbnailBlock'>
              {
                items.map((item) => {
                  return (
                    <div className='thumbnail'>
                      <div> <img id="thumbnail" src={item[this.props.name]} alt="" /></div>
                      <Div name="id" value={item.id} />
                      <Div name="title" value={item.title} />
                    </div>
                  )
                })
              }
            </div>
          </>
        );
      } else {
        return (
          <>
            <h1>Photos</h1>
            <div className='photoBlock'>
              {
                items.map((item) => {
                  return (
                    <div className='photos'>
                      <div> <img id="photos" src={item[this.props.name]} alt="" /></div>
                      <Div name="id" value={item.id} />
                      <Div name="title" value={item.title} />

                    </div>
                  )
                })
              }
            </div></>
        );
      }

    }
  }
}


