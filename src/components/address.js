import React, { Component } from 'react'
import Div from './div'

export default class address extends Component {

  render() {
    const { city, street, zipcode } = this.props.value;
    return (
      <div>
        <h4>Address:</h4>
        <Div name="city" value={city} />
        <Div name="street" value={street} />
        <Div name="zipcode" value={zipcode} />
      </div>
    )
  }
}

