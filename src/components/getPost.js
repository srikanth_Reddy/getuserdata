import React, { Component } from 'react'
import Div from './div';
import './getPosts.css';

export default class getPost extends Component {

  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }
  componentDidMount() {
    let Y = this.props.id;
    fetch(`https://jsonplaceholder.typicode.com/posts?userId=${Y}`)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result
          });
        },

        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return <div className='posts'>
        <h1>Posts:</h1><br />
        <div className='post'>
          {
            items.map((x) => (
              <div>
                <Div name="id" value={x.id} />
                <Div name="title" value={x.title} />
                <Div name="body" value={x.body} />
              </div>

            ))
          }
        </div>
      </div>
    }
  }
}

