import React from 'react';
const div = (props) => {
  const { name, value } = props;
  return (
    <div>{`${name} : ${value}`}</div>
  )
}
export default div;