import './App.css';
import DisplayUserData from './components/getUserData'

function App() {
  return (
    <div className="App">
      <DisplayUserData />
    </div>
  );
}

export default App;
